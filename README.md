# Assignment 3 Test repo. #

## How to run? ##
1. Start your server
2. Execute tests: java -jar test.jar ${ipAddress} ${port}
3. e.g. java -jar test.jar localhost 8080

## Test description ##
1. test Put Command: Bare-bones test. Only checks if error code == 0 for put req.
2. test Get Command: Put -> Get. Check that the values are equal + error code == 0
3. test Remove Command: Put -> Remove -> Get. Check that error == non exist key
4. test DeleteAll command: Put(key1) -> Put(Key2) -> DeleteAll -> Get(Key1) -> Get(Key2).
5. test IsAlive command: Check error code == 0
6. test GetPID command: Check error code == 0